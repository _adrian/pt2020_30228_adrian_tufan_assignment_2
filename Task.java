package assignment2;

public class Task implements Comparable<Task> {
    private int id;
    private int arrivalTime;
    private int processingTime;

    public Task(int id, int arrivalTime, int processingTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
    }

    public int getArrivalTime() {

        return arrivalTime;
    }

    public void setProcessingTime(int newProcessingTime) {
        this.processingTime = newProcessingTime;
    }

    public int getProcessingTime() {

        return processingTime;
    }

    @Override
    public int compareTo(Task o) {
        if (arrivalTime < o.getArrivalTime())
            return -1;
        else if (arrivalTime > o.getArrivalTime())
            return 1;
        return 0;
    }

    @Override
    public String toString() {
        return "(" + id + "," + arrivalTime + "," + processingTime + ")";
    }
}
