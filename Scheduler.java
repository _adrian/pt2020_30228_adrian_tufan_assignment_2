package assignment2;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;

    public Scheduler(int maxNoServers, int maxTasksPerServer) {
        servers = new ArrayList<Server>();
        this.maxNoServers = maxNoServers;
        changeStrategy(SelectionPolicy.SHORTEST_TIME);

        for (int i = 0; i < maxNoServers; i++) {
            Server server = new Server(maxTasksPerServer + 5);
            servers.add(server);
        }
    }

    public void changeStrategy(SelectionPolicy policy) {
        if (policy == SelectionPolicy.SHORTEST_TIME) {
            strategy = new ConcreteStrategyTime();
        }
    }

    public void dispatchTask(Task t) throws InterruptedException {
        strategy.addTask(servers, t);
    }

    public List<Server> getServers() {
        return servers;
    }
}
