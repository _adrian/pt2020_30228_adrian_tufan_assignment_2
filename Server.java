package assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable
{
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private int maxTasksPerServer;
    private static List<Integer> waitingTime;
    private boolean running;

    public Server(int maxTasksPerServer)
    {
        this.maxTasksPerServer = maxTasksPerServer;
        waitingPeriod = new AtomicInteger();
        waitingPeriod.set(0);
        tasks = new ArrayBlockingQueue<>(maxTasksPerServer);
        waitingTime = Collections.synchronizedList(new ArrayList<Integer>());
    }

    public boolean isRunning()
    {
        return running;
    }

    public void addTask(Task newTask) throws InterruptedException
    {
        if(tasks.isEmpty())
        {
            Thread th = new Thread(this);
            th.start();
            running = true;
        }

        tasks.put(newTask);
        waitingPeriod.getAndAdd(newTask.getProcessingTime());
        waitingTime.add(waitingPeriod.intValue());
    }

    public void run()
    {
        while(!tasks.isEmpty())
        {
            try
            {
                Task t = tasks.peek();
                if(t == null)
                    continue;
                while(t.getProcessingTime() > 0)
                {
                    Thread.sleep(1000);
                    t.setProcessingTime(t.getProcessingTime() - 1);
                    waitingPeriod.getAndAdd(-1);
                }
                tasks.take();
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        running = false;
    }

    public AtomicInteger getWaitingPeriod()
    {
        return waitingPeriod;
    }

    public Task[] getTasks()
    {
        int i = 0;
        Task[] t = new Task[tasks.size()];
        for(Task task: tasks)
        {
            t[i] = task;
            i++;
        }
        return t;
    }

    public List<Integer> getWaitingTime()
    {
        return waitingTime;
    }
}
