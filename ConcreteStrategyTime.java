package assignment2;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addTask(List<Server> servers, Task t) throws InterruptedException {
        int min = Integer.MAX_VALUE;
        for (Server s : servers) {
            int time = s.getWaitingPeriod().intValue();
            if (time < min) {
                min = time;
            }
        }

        for (Server s : servers) {
            if (s.getWaitingPeriod().intValue() == min) {
                s.addTask(t);
                return;
            }
        }
    }
}

