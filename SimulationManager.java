package assignment2;

import java.io.*;
import java.util.*;

public class SimulationManager implements Runnable {
    private int timeLimit;
    private int maxProcessingTime;
    private int minProcessingTime;
    private int numberOfServers;
    private int numberOfClients;
    private int minArrivalTime;
    private int maxArrivalTime;
    private Scheduler scheduler;
    private List<Task> generatedTasks;

    private String inputFilePath;
    private String outputFilePath;


    public SimulationManager(String inputFilePath, String outputFilePath) {
        this.inputFilePath = inputFilePath;
        this.outputFilePath = outputFilePath;
        generatedTasks = new ArrayList<Task>();
        generateRandomTasks();
        scheduler = new Scheduler(numberOfServers, numberOfClients / numberOfServers + 1);
    }

    public Scanner openInputFile(String path) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return scanner;
    }

    public void openOutputFile(String path) {
        File file = new File(path);
        try {
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void readFromFile(Scanner in) {
        if (in == null)
            return;

        String s;
        String[] r;

        numberOfClients = Integer.parseInt(in.next());
        numberOfServers = Integer.parseInt(in.next());
        timeLimit = Integer.parseInt(in.next());

        s = in.next();
        r = s.split(",");
        minArrivalTime = Integer.parseInt(r[0]);
        maxArrivalTime = Integer.parseInt(r[1]);

        s = in.next();
        r = s.split(",");
        minProcessingTime = Integer.parseInt(r[0]);
        maxProcessingTime = Integer.parseInt(r[1]);
    }

    public void generateRandomTasks() {
        Random rand = new Random();

        Scanner in = openInputFile(inputFilePath);
        readFromFile(in);

        for (int i = 1; i <= numberOfClients; i++) {
            int arrivalTime = (rand.nextInt() & Integer.MAX_VALUE) % (maxArrivalTime - minArrivalTime + 1) + minArrivalTime;
            int processingTime = (rand.nextInt() & Integer.MAX_VALUE) % (maxProcessingTime - minProcessingTime + 1) + minProcessingTime;
            generatedTasks.add(new Task(i, arrivalTime, processingTime));
        }
        Collections.sort(generatedTasks);
    }

    public void writeToFile(String filePath, String buffer) {
        try {
            FileWriter fileWriter = new FileWriter(filePath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            if (buffer != "\n")
                bufferedWriter.write(buffer);
            else
                bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeInfoAboutQueues() {
        int serverCount = 0;
        for (Server server : scheduler.getServers()) {
            serverCount++;
            writeToFile(outputFilePath, "\nQueue " + serverCount + ":");
            if (!server.isRunning())
                writeToFile(outputFilePath, " closed");
            else
                for (Task t : server.getTasks())
                    writeToFile(outputFilePath, t + ";");
        }
        writeToFile(outputFilePath, "\r\n");
    }

    public void writeInfoAboutAvgTime() {
        Server server = scheduler.getServers().get(0);
        double totalTime = 0;
        for (Integer i : server.getWaitingTime()) {
            totalTime += i.intValue();
        }

        double averageTime = totalTime / numberOfClients;
        writeToFile(outputFilePath, "\nAverage waiting time: " + averageTime);
    }

    public int parseTask(Task t, int currentTime) {
        int result = 0;
        if (t.getArrivalTime() <= currentTime) {
            try {
                scheduler.dispatchTask(t);
                generatedTasks.remove(t);
                result = 1;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else
            writeToFile(outputFilePath, t + ";");
        return result;
    }

    public void run() {
        int currentTime = 0, i;
        openOutputFile(outputFilePath);
        while (currentTime < timeLimit) {
            writeToFile(outputFilePath, (currentTime == 0 ? "Time " : "\nTime ") + currentTime);
            writeToFile(outputFilePath, "\nWaiting clients: ");

            i = 0;
            while (i < generatedTasks.size()) {
                int result = parseTask(generatedTasks.get(i), currentTime);
                if (result != 0)
                    i--;
                i++;
            }
            writeInfoAboutQueues();

            currentTime++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        writeInfoAboutAvgTime();
    }

    public static void main(String[] args) {
        SimulationManager manager = new SimulationManager(args[0], args[1]);
        Thread th = new Thread(manager);
        th.start();
    }
}
